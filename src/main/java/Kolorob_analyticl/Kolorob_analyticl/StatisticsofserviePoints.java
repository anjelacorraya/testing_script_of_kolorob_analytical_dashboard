package Kolorob_analyticl.Kolorob_analyticl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class StatisticsofserviePoints {
	
	
WebDriver driver;
	

	public StatisticsofserviePoints(WebDriver driver) {
				this. driver=driver;
		
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[@id=\"content\"]/div[2]/div[1]/div/section/div/div/div/div[1]/ul/li[1]/a/strong")
	WebElement Option_Statistics_of_service_point; 
	
	@FindBy (xpath = "//*[@id=\"panel36\"]/form/div[2]/div/div/div[1]/div")
	WebElement districtButton;
	
	@FindBy(xpath="/html/body/div[2]/div/div[2]/div[1]/div/section/div/div/div/div[2]/div[1]/form/div[2]/div/div/div[1]/div/ul/span/div/input")
	WebElement SearchforDistrict_Dhaka;
	
	@FindBy(xpath="/html/body/div[2]/div/div[2]/div[1]/div/section/div/div/div/div[2]/div[1]/form/div[2]/div/div/div[1]/div/ul/li[51]/span")
	WebElement Select_District_Dhaka;
	
	@FindBy(xpath="/html/body/div[2]/div/div[2]/div[1]/div/section/div/div/div/div[2]/div[1]/form/div[4]/div/button[2]")
	WebElement SearchButton;
	public void ClickOnStatistics_service_point() {
	Option_Statistics_of_service_point.click();
	}
	
	
	public void clickOnDistrictButton() {
	districtButton.click();
	}
	
	public void searchforDistrictDhaka(String disName) {
		SearchforDistrict_Dhaka.sendKeys(disName);;
	}


	public void selectDistrictDhaka() {
		Select_District_Dhaka.click();
		
	}


	public void clickOnSearchButton() {
		SearchButton.click();
	}

//	public void selectDistrictByVisibleText(String text) {
//		Select select = new Select(selectoptionsforDistrict);
//		select.selectByVisibleText(text);
//			
//	}
//	
	

}

